# Dotfiles

This is a collection of my personal dotfiles

- i3status for Status Bar
- i3wm for Window Manager
- feh for Backgrounds


## Hella Apt-get

```bash
sudo apt-get install cmake cmake-data libcairo2-dev libxcb1-dev libxcb-ewmh-dev libxcb-icccm4-dev libxcb-image0-dev libxcb-randr0-dev libxcb-util0-dev libxcb-xkb-dev pkg-config python-xcbgen xcb-proto libxcb-xrm-dev i3-wm libasound2-dev libmpdclient-dev libiw-dev libcurl4-openssl-dev libpulse-dev libxcb-composite0-dev xcb libxcb-ewmh2
```

## Mac OSX order of operations

```
brew/install_brew.sh
brew/install_brew_extensions.sh
brew/install_cask_extensions.sh
vscode/install_extensions.sh
Move `vscode/settings.json` to `~/Library/Application Support/Code/User/settings.json`
While any of these are going, download a new browser, slack, gitkraken, etc...
```
